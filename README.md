![](/img/duke/logo.png)

# The Drake - pravidla hry

The Drake je desková hra pro dva hráče inspirovaná úspěšnou hrou [The Duke](http://www.catalystgamelabs.com/casual-games/the-duke/). V podstatě jde o zjednodušenou verzi The Duke, která se hraje na menším hracím plánu s menším počtem figur a některá její pravidla jsou přizpůsobena tomu, aby se hra dobře programovala. The Drake má několik možných verzí podle různých úprav a rozšíření, o které je možné obohatit takzvanou základní verzi.

## Základní verze hry

Základní verze hry The Drake se hraje na čtvercové desce rozdělené na 4×4 polí.

![](/img/duke/board.png)

Hra začíná s prázdnou hrací deskou. Každý hráč má od začátku hry k dispozici takzvaný _zásobník_, který obsahuje předem stanovenou sadu hracích kamenů zvaných _jednotky_. Jednotka, která se na začátku hry nachází na vrcholu zásobníku, se označuje jako _vůdce_. Hráči, označovaní jako „modrý“ a „oranžový“ podle barvy svých jednotek, střídavě provádějí tahy, které mohou vyústit v zajmutí některé ze soupeřových jednotek na hrací desce. Hru vyhrává ten hráč, kterému se jako prvnímu podaří zajmout soupeřova vůdce.

## Jednotky

Základní verze hry obsahuje celkem šest druhů jednotek: Drake, Clubman, Monk, Spearman, Swordsman a Archer. Jednotky jsou dvou barev, modré nebo oranžové a každá má lícovou a rubovou stran. Každý hráč má po celou dobu hry pevně přidělenou barvu jednotek, se kterými hraje.

![](/img/duke/piece-set.png)

Každý hráč má na začátku hry zásobník sedmi jednotek v přesně tomto pořadí:

1.  Drake
2.  Clubman
3.  Clubman
4.  Monk
5.  Spearman
6.  Swordsman
7.  Archer

## Fáze hry

Hra The Drake sestává ze tří fází: _zahájení_, _stavění stráží_ a _střední hra_.

### Zahájení

Na počátku hry je hra ve fází zahájení. Během zahájení hráči rozmísťují svoje vůdce. Vůdce je jednotka, která se na začátku hry nachází na vrcholu zásobníku, což je v základní verzi hry vždy jednotka Drake. Hru vždy začíná modrý hráč, který může položit svého vůdce na kterékoliv pole na řadě 1 hrací desky. Následuje oranžový hráč, který může položit svého vůdce na kterékoliv pole řady 4\. Tímto hra přechází do fáze stavění stráží.

### Stavění stráží

Jednotky, které se na začátku hry nacházejí na druhé a třetí pozici v zásobníku, se nazývají _stráže_. V základní verzi hry jsou stráže vždy dvě jednotky Clubman. Na začátku stavění stráží je na tahu modrý hráč. Ten může položit první stráž z vrcholu zásobníku na kterékoliv prázdné pole, které se jednou hranou sousedí s jeho vůdcem. Podle stejných pravidel pak staví svoji první stráž oranžový hráč. Poté modrý hráč staví svoji druhou stráž, která se opět na volné pole, které se jednou hranou dotýká pole s vůdcem. Analogicky postupuje oranžový hráč s postavením svojí druhé stráže. Tímto hra přechází do fáze střední hry.

### Střední hra

Ve střední hře se modrý a oranžový hráč střídají v provádění tahů, jejichž podstata je vyložena v následujícím textu. Střední hra končí ve chvíli, kdy některý z hráčů vyhraje tím, že zajme soupeřova vůdce. Střední hra také může skončit remízou.

## Tah ve střední hře

Tahem se rozumí buď:

1.  Položení jednotky z vrcholu zásobníku na některé volné pole hrací desky. Hráč může položit jednotku pouze na volné pole, které alespoň jednou hranou sousedí s jednotkou jeho barvy. Jednotka se vždy na desku pokládá lícovou stranou nahoru.
2.  Provedení takzvané _akce_ jednotkou, která již leží na nějakém hracím poli. Každá jednotka má na svém líci i rubu vyobrazeny symboly představující akce. Ty určují, jakým způsobem lze jednotkou na hrací desce táhnout. Po provedení jakékoliv akce se jednotka vždy otočí na opačnou stranu, tedy z rubu na líc nebo z líce na rub.

## Akce

Akce na rubové a lícové straně jednotky udávají, jakým způsobem může jednotka táhnout po hrací desce. Každá jednotka má takzvaný _pivot_, který je označen symbolem pripomínajícím <tt>i</tt>. Pivot představuje políčko, na kterém zrovna jednotka stojí a je tak referenčním bodem pro všechny akce jednotky.

Základní verze hry The Drake obsahuje tři akce:

### ![](/img/duke/shift-icon.png)Akce krok

Na pole označené akcí krok může jednotka vstoupit nebo zde zajmout soupeřovu jednotku, pokud není přímá cesta na toto pole zablokována jiným polem, na které nelze vstoupit.

![](/img/duke/shift.png)

### ![](img/duke/slide-icon.png)Akce posun

Jednotka může vstoupit nebo zajmout soupeřovu jednotku na všechna pole ve směru šipky akce v případě, že přímá cesta k tomuto poli není zablokována jiným polem, na které nelze vstoupit.

![](/img/duke/slide.png)

### ![](/img/duke/strike-icon.png)Akce úder

Jednotka může zajmout soupeřovu jednotku, pokud soupeřova jednotka stojí na poli označeném touto akcí. %Úder je možné provést bez ohledu na to, zda je přímá cesta k cílovému poli volná či nikoliv. Jednotka po úderu zůstává na svém původním poli.

![](/img/duke/strike.png)

## Možná rozšíření

Základní verzi hry je možné různými způsoby rozšiřovat a upravovat:

*   Přidávat nové typy jednotek.
*   Přidávat nové typy akcí.
*   Přidávat takzvané _tokeny_. Token je symbol podobný jako akce. Na rozdíl od akce však zabraňuje soupeři v provedení nějakého tahu. Například token _protect_ může zabránit soupeři vstoupit na políčko sousedící s jednotkou. Token _dread_ naopak zabraňuje nějaké soupeřově jednotce pohnout se ze svého pole.
*   Přidávat speciální předměty, jako například vlajku, kterou je třeba někam přenést nebo bombu, která leží na zásobníku, a lze jí zničit nějakou soupeřovu jednotku, čímž bomba zanikne.
*   Přidávat nové typy políček. Například překážky, na které nelze vstoupit, pevnosti, do kterých se lze na jeden tah skrýt apod.
*   Pozměnit pořadí zásobníku tak, že vůdcem bude jiná jednotka než Drake. Mohou být také varianty, kdy se zásobníky jednotlivých hráčů od sebe liší.
*   Zavést do hry nedeterministický prvek tím, že zásobník se vytvoří na začátku hry náhodně. Zároveň může být hráčům skrytý, takže hráči vidí jen jeho vrchol.
*   Další varianta je losovat jednoty na vrcholu zásobníku zcela náhodně až během hry.