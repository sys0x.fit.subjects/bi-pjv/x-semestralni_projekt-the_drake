package thmw.thedrake;

public enum GameResult {
	VICTORY("VICTORY"), DRAW("DRAW"), IN_PLAY("IN_PLAY");

	private String value;

	GameResult(String value) { this.value = value; }

	@Override
	public String toString() { return value; }
}
