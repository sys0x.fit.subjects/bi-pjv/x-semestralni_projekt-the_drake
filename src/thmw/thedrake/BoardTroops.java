package thmw.thedrake;

import java.io.PrintWriter;
import java.util.*;

public class BoardTroops implements JSONSerializable{
	private final PlayingSide playingSide;
	private final Map<BoardPos, TroopTile> troopMap;
	private final TilePos leaderPosition;
	private final int guards;
	
	public BoardTroops(PlayingSide playingSide) { 
		this(playingSide, Collections.emptyMap(), TilePos.OFF_BOARD, 0);
	}

	public BoardTroops(PlayingSide playingSide, Map<BoardPos, TroopTile> troopMap, TilePos leaderPosition, int guards) {
		this.playingSide = playingSide;
		this.troopMap = troopMap;
		this.leaderPosition = leaderPosition;
		this.guards = guards;
	}

	public Optional<TroopTile> at(TilePos pos) {
		return Optional.ofNullable(troopMap.get(pos));
	}
	
	public PlayingSide playingSide() {
		return playingSide;
	}
	
	public TilePos leaderPosition() {
		return leaderPosition;
	}

	public int guards() {
		return  guards;
	}
	
	public boolean isLeaderPlaced() {
		return leaderPosition == TilePos.OFF_BOARD ? false : true;
	}
	
	public boolean isPlacingGuards() {
		if (isLeaderPlaced() && guards < 2) { return true; }
		else { return false; }
	}	
	
	public Set<BoardPos> troopPositions() {
		return troopMap.keySet();
	}

	public BoardTroops placeTroop(Troop troop, BoardPos target) {

		if(at(target).isPresent()) {
			throw new IllegalArgumentException();
		}

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.put(target, new TroopTile(troop, playingSide, TroopFace.AVERS));

		if (!isLeaderPlaced()) {
			return new BoardTroops(playingSide, newTroops, target, guards);
		}
		if (isPlacingGuards()) {
			return new BoardTroops(playingSide, newTroops, leaderPosition, guards + 1);
		}
		return new BoardTroops(playingSide, newTroops, leaderPosition, guards);

	}
	
	public BoardTroops troopStep(BoardPos origin, BoardPos target) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");
		}

		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");
		}

		if (!at(origin).isPresent() || at(target).isPresent()) {
			throw new IllegalArgumentException();
		}

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.put(target, at(origin).get());
		newTroops.remove(origin);

		if(origin.equalsTo(leaderPosition.i(), leaderPosition.j())) {
			return new BoardTroops(playingSide, newTroops, target, guards);
		}

		return new BoardTroops(playingSide, newTroops, leaderPosition, guards);
	}
	
	public BoardTroops troopFlip(BoardPos origin) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");			
		}
		
		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");			
		}
		
		if(!at(origin).isPresent())
			throw new IllegalArgumentException();
		
		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		TroopTile tile = newTroops.remove(origin);
		newTroops.put(origin, tile.flipped());

		return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
	}
	
	public BoardTroops removeTroop(BoardPos target) {
		if(!isLeaderPlaced()) {
			throw new IllegalStateException(
					"Cannot move troops before the leader is placed.");
		}

		if(isPlacingGuards()) {
			throw new IllegalStateException(
					"Cannot move troops before guards are placed.");
		}

		if(!at(target).isPresent())
			throw new IllegalArgumentException();

		Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
		newTroops.remove(target);

		if (target.equalsTo(leaderPosition.i(), leaderPosition.j())) {
			return new BoardTroops(playingSide, newTroops, TilePos.OFF_BOARD, guards);
		}

		return new BoardTroops(playingSide, newTroops, leaderPosition, guards);

	}

	@Override
	public void toJSON(PrintWriter writer) {
		writer.print("\"boardTroops\":{\"side\":\"" + playingSide + "\"," +
					 "\"leaderPosition\":\"" + leaderPosition + "\"," +
					 "\"guards\":" + guards + ",");
		mapToJSON(writer); writer.print("}");
	}

	private void mapToJSON(PrintWriter writer) {
		Map<BoardPos, TroopTile> sortedMap = new TreeMap<>(troopMap);

		writer.print("\"troopMap\":{");
		boolean flag = false;
		for (Map.Entry<BoardPos, TroopTile> entry : sortedMap.entrySet()) {
			if (flag) writer.print(","); flag = true;
			writer.print("\"" + entry.getKey() + "\":{");
			entry.getValue().toJSON(writer);
			writer.print("}");
		}
		writer.print("}");
	}
}
