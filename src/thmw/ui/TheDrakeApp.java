package thmw.ui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import thmw.thedrake.*;

import java.util.stream.Collectors;


public class TheDrakeApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        GameState gameState = createSampleGameState();

        PositionFactory positionFactory= gameState.board().positionFactory();

        Label blueStackLbl = new Label("Blue Stack: ");
        Label orangeStackLbl = new Label("Orange Stack: ");
        Label blueCapburedLbl = new Label("Blue Capcured: ");
        Label orangeCapcuredLbl = new Label("Orange Capcured: ");

        ListView<String> BlueStack =new ListView<String>();
        BlueStack.setOrientation(Orientation.VERTICAL);
        BlueStack.setMouseTransparent( true );
        BlueStack.setFocusTraversable( false );

        ListView<String> OrangeStack = new ListView<String>();
        OrangeStack.setOrientation(Orientation.VERTICAL);
        OrangeStack.setMouseTransparent( true );
        OrangeStack.setFocusTraversable( false );

        ListView<String> OrangeCapcured = new ListView<String>();
        OrangeCapcured.setOrientation(Orientation.VERTICAL);
        OrangeCapcured.setMouseTransparent( true );
        OrangeCapcured.setFocusTraversable( false );

        ListView<String> BlueCapcured = new ListView<String>();
        BlueCapcured.setOrientation(Orientation.VERTICAL);
        BlueCapcured.setMouseTransparent( true );
        BlueCapcured.setFocusTraversable( false );

        VBox blueStackBox = new VBox();
        blueStackBox.setSpacing(10);
        blueStackBox.getChildren().addAll(blueStackLbl,BlueStack);
        blueStackBox.setMaxWidth(180);

        VBox orangeStackBox = new VBox();
        orangeStackBox.setSpacing(10);
        orangeStackBox.setMaxWidth(180);
        orangeStackBox.getChildren().addAll(orangeStackLbl,OrangeStack);

        VBox blueCapcuredBox = new VBox();
        blueCapcuredBox.setSpacing(10);
        blueCapcuredBox.getChildren().addAll(blueCapburedLbl,BlueCapcured);
        blueCapcuredBox.setMaxWidth(180);

        VBox orangeCapcuredBox = new VBox();
        orangeCapcuredBox.setSpacing(10);
        orangeCapcuredBox.getChildren().addAll(orangeCapcuredLbl,OrangeCapcured);
        orangeCapcuredBox.setMaxWidth(180);

        BoardView boardView = new BoardView(gameState, BlueStack, OrangeStack, BlueCapcured, OrangeCapcured);
        GridPane pane = new GridPane();
        pane.setHgap(10);
        pane.setVgap(5);
        pane.setPadding(new Insets(15));

        pane.add( blueStackBox,1,1);
        pane.add( blueCapcuredBox,1,2);
        pane.add( orangeStackBox,2,1);
        pane.add( orangeCapcuredBox,2,2);
        pane.add( boardView,0,0,1,3);

        primaryStage.setScene(new Scene(pane, 850, 600));
        primaryStage.setTitle("The Drake");

        primaryStage.show();
    }




    private static GameState createSampleGameState() {
        Board board = new Board(4);
        PositionFactory positionFactory = board.positionFactory();
        board = board.withTiles(new Board.TileAt(positionFactory.pos(1, 1), BoardTile.MOUNTAIN));
        return new StandardDrakeSetup().startState(board);
    }

}
