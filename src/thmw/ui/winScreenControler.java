package thmw.ui;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import thmw.ui.*;

public class winScreenControler {

    @FXML
    private javafx.scene.control.Button okButton;

    @FXML
    private void onOK(){
        // get a handle to the stage
        Stage stage = (Stage) okButton.getScene().getWindow();
        // do what you have to do
        stage.close();
        StartScreen newScreen=new StartScreen();
        try {
            newScreen.start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
