package thmw.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import thmw.thedrake.PlayingSide;

public class WinScreen extends Application {
    String winMessage ="DRAW";
    String bgColor="#00FF00";
    void wonSide(PlayingSide side){
        if(side==PlayingSide.ORANGE){
            winMessage ="ORANGE WON ";
            bgColor="#ffa500";

        }else {
            winMessage = "BLUE WON ";
            bgColor="#4885ed";
        }
    }
    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("WinScreen.fxml"));
        Label lblData = (Label) root.lookup("#lblTest");
        lblData.setText(winMessage);

        BorderPane brPane= (BorderPane) root.lookup("#border");
        brPane.setStyle("-fx-background-color:"+bgColor);

        primaryStage.setTitle("The Drake");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
